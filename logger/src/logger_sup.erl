-module(logger_sup).
-behavior(supervisor).
-export([init/1, start_link/0]).

start_link() ->
    supervisor:start_link(?MODULE, []).

init(_) ->
    {ok, {{one_for_one, 5, 10},
          [{logger_serv,
           {logger_serv, start_link, [self()]},
           permanent,
           10000,
           worker,
            [logger_serv]},
           {logger_worker_supervisor,
            {logger_worker_supervisor, start_link, []},
            permanent,
            15000,
            supervisor,
            [logger_worker_supervisor]}
          ]
         }}.
