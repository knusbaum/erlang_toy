-module(logger_serv).
-behavior(gen_server).
-export([start_link/1, init/1, handle_call/3, handle_cast/2,
         handle_info/2, terminate/2, code_change/3]).


start_link(Super) ->
    gen_server:start_link({local, logger}, ?MODULE, [Super], []).

init([Super]) ->
    process_flag(trap_exit, true),
    % If we have no log file open, we have to check if the file exists
    % and move it to a timestamped version so it doesn't get truncated.
    case error_logger:logfile(filename) of
        {error, no_log_file} ->
            case filelib:is_file("logger.log") of 
                true -> 
                    Suffix = make_timestamp(),
                    ok = file:rename("logger.log", "logger.log." ++ Suffix);
                false -> ok
            end;
        _ -> ok
    end,
    
    % next, turn off tty and open the log.
    ok = error_logger:tty(false),
    case error_logger:logfile({open, "logger.log"}) of
        ok -> ok;
        {error, allready_have_logfile} -> ok
    end,
    error_logger:info_report(["System Logging starting up."]),
    {ok, Super, 10000}.

handle_call({start_logger, Name, Filename}, _From, Super) ->
    % This is kind of expensive. Maybe eventually I can cache the
    % worker supervisor, but this call should be made very rarely,
    % so I don't think it really matters. The process hibernates 
    % when not in use, so that would probably be the first thing to
    % change if speed is an issue.
    error_logger:info_report(["Starting Logger", Name, Filename]),
    Worker_Super = 
        lists:filter(fun(Elem) ->
                             case Elem of
                                 {logger_worker_supervisor, _, _, _} -> true;
                                 _ -> false
                             end end,
                     supervisor:which_children(Super)),
    error_logger:info_report(["Worker_Super", Worker_Super]),
    case Worker_Super of
        [{_,Pid,_,_}] -> supervisor:start_child(Pid, [Name, Filename]),
                         error_logger:info_report(["Started Child.", Name, Filename]),
                         {reply, ok, Super, 5000};
        _ -> {reply, no_supervisor, Super, 5000}
    end;
handle_call(shutdown, _From, Super) ->
    exit(Super, shutdown),
    {reply, ok, Super};
handle_call(_,_,Super) ->
    {noreply, Super, 5000}.


handle_cast(_,Super) ->
    {noreply, Super, 5000}.


handle_info({'EXIT', Pid, Reason}, Super) ->
    error_logger:info_report(["System Logging got shutdown: ", 
                              {reason, Reason}, 
                              {from, Pid}]),
    {stop, Reason, Super};

handle_info(timeout, Super) ->
    error_logger:info_report(["System Logging hibernating."]),
    {noreply, Super, hibernate};

handle_info(Message, Super) ->
    error_logger:info_report(["System Logging got unexpected message: ",
                              {message, Message}]),
    {noreply, Super, 5000}.


terminate(shutdown, _) ->
    error_logger:info_report(["System Logging is shutting down:", 
                              {reason, shutdown}]),
    ok = error_logger:tty(true),
    ok = error_logger:logfile(close),
    ok;

terminate(Reason, _) ->
    error_logger:info_report(["System Logging is shutting down:", 
                              {reason, Reason}]),
    ok.


code_change(_,Super,_) ->
    {ok, Super}.
    

%% Private

make_timestamp() ->
    {{Year, Month, Day},{Hour,Minutes,Seconds}} = erlang:localtime(),
    io_lib:format("~2..0B~2..0B~B~2..0B~2..0B~2..0B", 
                  [Month, Day, Year, Hour, Minutes, Seconds]).
