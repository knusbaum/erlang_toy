-module(logger).
-behavior(application).
-export([start/2, start_logger/2, log/3,
        stop_logger/1, stop/1]).


start(normal, _Args) ->
    logger_sup:start_link().

start_logger(Name, File) ->
    gen_server:call(logger, {start_logger, Name, File}).

log(Name, Message, Args) ->
    logger_worker:log(Name, Message, Args).

stop_logger(Name) ->
    exit(whereis(Name), shutdown).

stop(_) ->
    gen_server:call(logger, shutdown),
    ok.

