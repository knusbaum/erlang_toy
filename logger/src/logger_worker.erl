-module(logger_worker).
-behavior(gen_server).
-export([start_link/2, init/1, handle_call/3, handle_cast/2,
         handle_info/2, terminate/2, code_change/3, log/3]).

log(Name, Message, Args) ->
    gen_server:cast(whereis(Name), {log, Message, Args}).


start_link(Name, Logfile) ->
    Res = gen_server:start_link({local, Name}, ?MODULE, [Logfile], []),
    error_logger:info_report(["Starting Logger: ", {name, Name}, {result, Res}]),
    Res.

init([Logfile]) ->
    process_flag(trap_exit, true),
    case file:open(Logfile, [append]) of
        {ok, Dev} -> 
            write_log(Dev, "Starting Logger."),
            {ok, Dev};
        {error, Reason} -> {stop, Reason}
    end.

handle_call({log, Message, Args}, _From, Dev) ->
    write_log(Dev, Message, Args),
    {reply, ok};
handle_call(terminate, _From, Dev) ->
    {stop, normal, ok, Dev}.

handle_cast({log, Message, Args}, Dev) ->
    write_log(Dev, Message, Args),
    {noreply, Dev}.

handle_info({'EXIT', Pid, Reason}, Dev) ->
    write_log(Dev, "Got EXIT from ~p: ~p", [Pid, Reason]),
    {stop, Reason, Dev};
handle_info(Message, Dev) ->
    write_log(Dev, "Unexpected Message: ~p", [Message]),
    {noreply, Dev}.
    
terminate(normal, Dev) ->
    write_log(Dev,"Normal Shutdown."),
    file:close(Dev),
    ok;
terminate(shutdown, Dev) ->
    write_log(Dev,"Got Shutdown from Supervisor."),
    file:close(Dev),
    ok;
terminate(Reason, Dev) ->
    write_log(Dev, "Abnormal Shutdown: ~p.", [Reason]),
    file:close(Dev),
    ok.

code_change(OldVsn, Dev, _Extra) ->
    %% No change planned. The function is here for the behavior,
    %% but will not be used. Only next version.
    write_log(Dev, "Loading new version. Old version: ~p~n", [OldVsn]),
    {ok, Dev}.


%%% Private Functions

make_datestring() ->
    {{Year, Month, Day},{Hour,Minutes,Seconds}} = erlang:localtime(),
    io_lib:format("[~B,~B,~B ~B:~B:~B]", [Month, Day, Year, Hour, Minutes, Seconds]).

write_log(Dev, Message) ->
    write_log(Dev, Message, []).

write_log(Dev, Message, Args) ->
    Date = make_datestring(),
    Log = io_lib:format(Message, Args),
    file:write(Dev, io_lib:format("~s: ~s~n", [Date, Log])).
